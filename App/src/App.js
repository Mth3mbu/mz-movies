import './App.css';
import axios from 'axios';
import Spinner from '../src/components/spinner/Spinner';
import Home from "../src/components/home/Home";
import React, { useState } from 'react'
import { BrowserRouter } from 'react-router-dom';
import ErrorSnackbar from '../src/components/snackbar/ErrorSnackBar';

function App() {

  const [isOpen, toggleSpinner] = useState({ open: false, openSnackbar: false });

  const handleClick = (event) => {
    toggleSpinner({ open: false, openSnackbar: false });
  };

  axios.interceptors.request.use(request => {
    toggleSpinner({ open: true, openSnackbar: false });
    return request;
  }, error => {
    toggleSpinner({ open: false, openSnackbar: true });
  })

  axios.interceptors.response.use(response => {
    toggleSpinner({ open: false, openSnackbar: false });
    return response;
  }, error => {
    toggleSpinner({ open: false, openSnackbar: true });
    return Promise.reject(error);
  })

  return (
    <BrowserRouter>
      <div className="App">
        <Spinner isOpen={isOpen.open} />
        <ErrorSnackbar openSnackbar={isOpen.openSnackbar} handleClose={handleClick}/>
        <Home />
      </div>
    </BrowserRouter>
  );
}

export default App;
