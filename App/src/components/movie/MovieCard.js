import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import FavoriteIcon from '@material-ui/icons/Favorite';
import QierPlayer from 'qier-player';
import Button from '@material-ui/core/Button';
import '../movie/Card.css'

const useStyles = makeStyles((theme) => ({
    root: {
        maxWidth: 740,
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    }, margin: { margin: 'auto' },
    expand: {
        transform: 'rotate(0deg)',
        marginLeft: 'auto',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
    avatar: {
        backgroundColor: red[500],
    },
}));

export default function MovieCard(props) {
    const classes = useStyles();

    const handleBackClick = () => {
       props?.route?.goBack();
    }

    return (
        <div className="card">
            <Card className={classes.root}>
                <QierPlayer className={classes.media} srcOrigin="https://vortesnail.github.io/qier-player-demo/static/media/video720p.fc81bd78.mp4" />
                <CardContent>
                    <Typography variant="body2" color="textSecondary" component="p">
                        Popularity:{`${props.movie.popularity}`}
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                         votes: {props.movie.vote_count}
                         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                         Released: {props.movie.first_air_date ?? '2021-01-05'}
                    </Typography>
                </CardContent>
                <CardActions disableSpacing>
                    <IconButton aria-label="share">
                        <FavoriteIcon />
                    </IconButton>
                </CardActions>
            </Card>
            <h2>{props.movie.title ?? props.movie.name}</h2>

            <Typography paragraph>
                {props.movie.overview}
            </Typography>

            <Button variant="contained" color="primary" onClick={handleBackClick}>
                Back
           </Button>
        </div>
    );
}