import { Component } from "react";
import '../movie/Movie.css';
import MovieCard from '../movie/MovieCard';
import { withRouter } from "react-router";
import { findMovieById } from '../../store/actions/actions';
import { connect } from 'react-redux';
class DetailsComponent extends Component {
    state = { movie: {} }
    componentDidMount() {
        const movie = this.props?.movies[this.props.selectedTabIndex]?.find(movie => {
            return movie.id === Number.parseInt(this.props.match.params.id) ? movie : null;
        })
        this.props.onFindMovieById(movie);
    }

    render() {
        return (<div className="movie-details-container">
            <div className="movieCard">
                <MovieCard movie={this.props.movie} route={this.props.history} />
            </div>
        </div>)
    }
}

const matchStateToProps = state => {
    return {
        movie: state.movie,
        movies: state.movies,
        selectedTabIndex: state.value
    }
}

const mapActionsToProps = dispatch => {
    return {
        onFindMovieById: (movie) => dispatch(findMovieById(movie))
    }
}
export default connect(matchStateToProps, mapActionsToProps)(withRouter(DetailsComponent));