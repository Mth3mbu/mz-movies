import React from 'react';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import { makeStyles } from '@material-ui/core/styles';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    '& > * + *': {
      marginTop: theme.spacing(2),
    },
  },
}));

export default function ErrorSnackbar(props) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Snackbar open={props.openSnackbar}  onClose={props.handleClose}>
        <Alert onClose={props.handleClose} severity="error">
          Ooops! something went wrong while attempting to connect to the server?
        </Alert>
      </Snackbar>
    </div>
  );
}