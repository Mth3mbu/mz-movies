import React, { } from 'react';
import PropTypes from 'prop-types';
import Box from '@material-ui/core/Box';
import Movie from '../movie/Movie';
import '../movies/Movies.css';
import '../movie/Movie.css';
import { connect } from 'react-redux';

const movies = (props) => {
    function TabPanel(props) {
        const { children, value, index, ...other } = props;
        return (
            <div
                role="tabpanel"
                hidden={value !== index}
                id={`scrollable-prevent-tabpanel-${index}`}
                aria-labelledby={`scrollable-prevent-tab-${index}`}
                {...other}
            >
                {value === index && (
                    <Box div={3}>
                        {children}
                    </Box>
                )}
            </div>
        );
    }

    TabPanel.propTypes = {
        children: PropTypes.node,
        index: PropTypes.any.isRequired,
        value: PropTypes.any.isRequired,
    };

    let movies = null;
    if (props.movies[props.value].length > 0) {
        movies = props.movies[props.value].map(movie => {
            return <Movie cssClass='movie' key={movie.id} title={movie.original_name == null ? movie.original_title : movie.original_name}
                overview={movie?.overview?.length <= 120 ? movie.overview : `${movie?.overview?.substring(1, 120)}....`}
                id={movie.id}
                image={`https://image.tmdb.org/t/p/w500/${movie?.backdrop_path}`} />
        });
    } else {
        movies = <p>No match found!</p>
    }


    return (<TabPanel value={props.value} index={props.index}>
        <div className='container'>
            {movies}
        </div>
    </TabPanel>)

}


const mapStateToProps = state => {
    return {
        movies: state.movies,
    }
}

export default connect(mapStateToProps)(movies);