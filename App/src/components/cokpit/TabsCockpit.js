import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import { FaBraille, FaParachuteBox, FaRegHeart, FaVideo } from "react-icons/fa";

const tabsCockpit = (props) => {

    const tabs = [{ name: 'Now Playing', icon: <FaVideo /> }, { name: 'Trending', icon: <FaBraille /> },
    { name: 'Popular', icon: <FaParachuteBox /> }, { name: 'Top Rated', icon: <FaRegHeart /> }]

    const moviesTabs = tabs.map((tab, index) => {
        return <Tab label={tab.name} icon={tab.icon} key={index} />
    })

    return <Tabs
        value={props.value}
        onChange={props.changed}
        indicatorColor="primary"
        textColor="primary"
        centered>

        {moviesTabs}
    </Tabs>
}

export default tabsCockpit;