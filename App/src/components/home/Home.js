import React, { Component, Fragment } from 'react';
import Paper from '@material-ui/core/Paper';
import AppBar from '../navigation/AppBar';
import Movies from "../movies/Movies";
import MoviesTab from '../cokpit/TabsCockpit';
import { Route, Switch } from 'react-router-dom';
import DetailsComponent from '../movie/Details';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { fetchMovies, storeSelectedTabIndex } from '../../store/actions/actions';

class Home extends Component {
    state = { value: 0 };

    componentDidMount() {
        this.props.onfetchMovies();
    };

    render() {
        const handleChange = (event, newValue) => {
            this.props.onTabChange(newValue);
            this.setState({
                value: newValue
            })
            this.props.history.push('/');
        };

        return (
            <Fragment>
                <div className="nav">
                    <AppBar />
                    <Paper >
                        <MoviesTab value={this.state.value} changed={handleChange} />
                    </Paper>
                </div>
                <div className="spacer">
                    &nbsp;
               </div>
                <Switch>
                    <Route path="/" render={() => this.props.movies.map((movie, index) => {
                        return <Movies value={this.state.value} index={index} key={index} />
                    })} exact />
                    <Route path="/:id" exact component={DetailsComponent} />
                </Switch>
            </Fragment>
        )
    }
}

const mapDispachActionsToProps = dispatch => {
    return {
        onfetchMovies: () => dispatch(fetchMovies()),
        onTabChange: (index) => dispatch(storeSelectedTabIndex(index))
    }
}

const mapStateToProps = state => {
    return {
        selectedTabIndex: state.value,
        movies: state.movies,
        value: state.value
    }
}

export default connect(mapStateToProps, mapDispachActionsToProps)(withRouter(Home));