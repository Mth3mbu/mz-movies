const tabReducer = (state = intitialState, action) => {
    intitialState = {};
    switch (action.type) {
        case actionTypes.FETCHMOVIES: return {
            ...state,
            movies: []
        }

        case actionTypes.SEARCH: {
            return {
                ...state,
                movies: [],
                allMovies: []
            }
        }
        default:
            return state;
    }
}

