import * as actionTypes from '../actions/actionTypes';

const initialState = { value: 0, movies: [], allMovies: [], movie: {} }

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCHMOVIES:
            return {
                ...state,
                movies: action.movies,
                allMovies: action.backupMovies
            }
        case actionTypes.SELECTEDTAB:
            return {
                ...state,
                value: action.selectedTabIndex
            }
        case actionTypes.FINDBYID: {
            return {
                ...state,
                movie: action.movie
            }
        }
        case actionTypes.SEARCH: {
            const searchTerm = action.event.target.value.toLowerCase();
            const moviesState = { ...state };
            let filterResults = state.movies[state.value]?.filter(movie => {
                let title = movie.original_name == null ? movie.original_title : movie.original_name;
                return title?.toLowerCase()?.includes(searchTerm);
            });

            if (searchTerm === '') {
                filterResults = state.allMovies[state.value];

            }
            moviesState.movies[state.value] = filterResults;

            return {
                ...state,
                movies: [...moviesState.movies]
            }
        }
        default:
            break
    }

    return state;
}

export default reducer;