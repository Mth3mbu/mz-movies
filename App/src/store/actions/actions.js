import axios from 'axios';
import * as actionTypes from './actionTypes';

const urls = ['/trending/all/day?api_key=9e7e9ca6ac1f622c9128828848321b70&language=en-US&page=1',
    '/tv/airing_today?api_key=9e7e9ca6ac1f622c9128828848321b70&language=en-US&page=1',
    '/movie/popular?api_key=9e7e9ca6ac1f622c9128828848321b70&language=en-US&page=1',
    '/movie/top_rated?api_key=9e7e9ca6ac1f622c9128828848321b70&language=en-US&page=1'];

export const fetchMovies = () => {
    return function (dispatch) {
        const requests = urls.map(request => {
            return axios.get(request);
        });

        axios.all([requests[0], requests[1], requests[2], requests[3]]).then(axios.spread((...responses) => {
            const movies = responses.map(response => {
                return response.data.results;
            });
            dispatch(storeMovies(movies));
        }));
    }
}

export const storeSelectedTabIndex = (index) => {
    return {
        type: actionTypes.SELECTEDTAB,
        selectedTabIndex: index
    }
}

export const findMovieById = (selectedMovie) => {
    return {
        type: actionTypes.FINDBYID,
        movie: {...selectedMovie}
    }
}

export const storeMovies = (results) => {
    return {
        type: actionTypes.FETCHMOVIES,
        movies: results,
        backupMovies: [...results]
    }
}


