package com.example.app;

public class MoviesResult {
    public int [] genre_ids;
    public int id;
    public double popularity;
    public int vote_count;
    public String original_name;
    public String original_language;
    public String original_title;
    public String title;
    public String poster_path;
    public String backdrop_path;
    public String media_type;
    public String overview;

    public boolean video;
    public boolean adult;

    public double vote_average;

    public String release_date;

    public MoviesResult(){}
    public MoviesResult(int[] genre_ids, int id, double popularity, int vote_count, String original_language
    , String original_title, String title, String poster_path, String backdrop_path, String media_type, String overview,
                        boolean video, boolean adult, String release_date, String original_name){
        this.genre_ids =genre_ids;
        this.id=id;
        this.popularity =popularity;
        this.vote_count = vote_count;
        this.original_language =original_language;
        this.original_title =original_title;
        this.title =title;
        this.poster_path =poster_path;
        this.backdrop_path = backdrop_path;
        this.media_type =media_type;
        this.overview =overview;
        this.video =video;
        this.adult =adult;
        this.release_date =release_date;
        this.original_name =original_name;
    }
}
