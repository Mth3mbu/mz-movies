package com.example.app.models;

public class Details {
      public boolean adult;
      public String backdrop_path;
      public int budget;
      public String homepage;
      public int id;
      public int imdb_id;
      public String original_language;
      public String original_title;
      public String overview;
      public double popularity;
      public String poster_path;
      public String release_date;
      public double revenue;
      public int runtime;
      public String status;
      public String tagline;
      public String title;
      public boolean video;
      public double vote_average;
      public int vote_count;

      public Details( boolean adult,String backdrop_path,int budget,String homepage,int id,int imdb_id,String original_language,
                      String original_title, String overview,double popularity,String poster_path, String release_date,double revenue,
                      int runtime, String status,String title, String tagline, boolean video, double vote_average, int vote_count){
          this.adult =adult;
          this.backdrop_path =backdrop_path;
          this.budget =budget;
          this.homepage =homepage;
          this.id =id;
          this.imdb_id =imdb_id;
          this.original_language =original_language;
          this.original_title =original_title;
          this.overview =overview;
          this.popularity= popularity;
          this.poster_path =poster_path;
          this.release_date =release_date;
          this.revenue =revenue;
          this.runtime =runtime;
          this.status =status;
          this.title =title;
          this.tagline =tagline;
          this.video =video;
          this.vote_average =vote_average;
          this.vote_count =vote_count;
      }

      public Details(){

      }
}
