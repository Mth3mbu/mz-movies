package com.example.app.adapters
import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.example.app.DetailsActivity
import com.example.app.MoviesResult
import com.example.app.R
import com.example.app.state.MovieState
import com.squareup.picasso.Picasso


class MovieAdapter(private  val movies: Array<MoviesResult>, private val activity: Activity): RecyclerView.Adapter<MovieAdapter.MovieViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.movie, parent,false)
        return MovieViewHolder(itemView,movies,activity)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
    val currentMovie =movies[position]
        val imageUri ="https://image.tmdb.org/t/p/w500/"+ currentMovie.poster_path
        Picasso.with(activity).load(imageUri).into(holder.imageView)
    }

    override fun getItemCount(): Int {
      return movies.size
    }

    class MovieViewHolder(itemView: View, movies: Array<MoviesResult>,activity: Activity): RecyclerView.ViewHolder(itemView){
        val imageView :ImageView = itemView.findViewById(R.id.imageView)

        init {
            itemView.setOnClickListener{
                run {
                    val index: Int = adapterPosition
                    val intent = Intent(activity, DetailsActivity::class.java)
                     MovieState.setSelectedMovie(movies[index])
                     activity.startActivity(intent)
                }
            }
        }
    }
}