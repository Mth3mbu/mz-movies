package com.example.app.state
import com.example.app.MoviesResult

class MovieState {
     companion object{
         private var movie:MoviesResult = MoviesResult()
         fun setSelectedMovie(selectedMovie: MoviesResult){
             movie = selectedMovie
         }

         fun getSelectedMovie(): MoviesResult = movie
     }
}