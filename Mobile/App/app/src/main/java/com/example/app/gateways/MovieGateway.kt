package com.example.app.gateways
import com.example.app.models.Movie
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import com.google.gson.Gson

class MovieGateway {

    private var movies = Movie(0, null, 0, 0)

    fun getMovies(url:String): Movie {
        val httpAsync = url
            .httpGet()
            .responseString { _, _, result: Result<String, FuelError> ->
                when (result) {
                    is Result.Failure -> {
                        val ex = result.getException()
                    }
                    is Result.Success -> {
                        movies = Gson().fromJson(result.get(), Movie::class.java)
                    }
                }
            }
        httpAsync.join()

        return movies
    }
}