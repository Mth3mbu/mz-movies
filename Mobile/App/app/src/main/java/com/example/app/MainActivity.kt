package com.example.app
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.example.app.adapters.ViewPagerAdapter
import com.example.app.fragments.HomeFragment
import com.example.app.fragments.NowPlayingFragment
import com.example.app.fragments.PopularFragment
import com.example.app.fragments.TrendingFragment
import com.google.android.material.tabs.TabLayout

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setUpTabs()
    }

    private fun setUpTabs(){
        val adapter = ViewPagerAdapter(supportFragmentManager)
        val viewPager =findViewById<ViewPager>(R.id.viewPager)
        val tabs =findViewById<TabLayout>(R.id.tabs)

        adapter.addFragment(NowPlayingFragment(this), "Now Playing")
        adapter.addFragment(HomeFragment(this), "All")
        adapter.addFragment(PopularFragment(this), "Popular")
        adapter.addFragment(TrendingFragment(this), "Top Rated")
        viewPager.adapter =adapter
        tabs.setupWithViewPager(viewPager)

        tabs.getTabAt(0)!!.setIcon(R.drawable.live)
        tabs.getTabAt(1)!!.setIcon(R.drawable.new_popular)
        tabs.getTabAt(2)!!.setIcon(R.drawable.favorite)
        tabs.getTabAt(3)!!.setIcon(R.drawable.trending)
    }
}