package com.example.app.fragments

import android.app.Activity
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.app.adapters.MovieAdapter
import com.example.app.R
import com.example.app.gateways.MovieGateway

class PopularFragment(private val activity: Activity) : Fragment() {
    private lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_popular, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView = view.findViewById(R.id.popularMoviesRecyclerView)
        renderMovies()
    }

    private fun renderMovies(){
        val url = "https://api.themoviedb.org/3/movie/popular?api_key=9e7e9ca6ac1f622c9128828848321b70&language=en-US&page=2"
        val movies = MovieGateway().getMovies(url)
        val gridLayout= GridLayoutManager(activity,3, GridLayoutManager.VERTICAL,false)
        recyclerView.layoutManager =gridLayout
        recyclerView.adapter = MovieAdapter(movies.results,activity)
        recyclerView.setHasFixedSize(true)
    }
}
