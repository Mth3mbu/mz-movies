package com.example.app.models

data class MovieItem (val imageResource: Int, val title : String, val description: String)
