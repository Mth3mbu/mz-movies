package com.example.app

import android.app.Activity
import android.app.AlertDialog

class Loader (private val activity: Activity) {
    private var dialog: AlertDialog? =null;

    public fun showDialog(){
        val builder = AlertDialog.Builder(activity);
        val inflater = activity.layoutInflater;
        builder.setView(inflater.inflate(R.layout.loader,null))
        builder.setCancelable(false);
        dialog = builder.create();
        dialog?.show()
    }

    public fun dismissDialog(){
        dialog?.dismiss();
    }
}
