package com.example.app
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import com.example.app.state.MovieState
import com.squareup.picasso.Picasso

class DetailsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        val movie = MovieState.getSelectedMovie()

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.title = movie.title?:movie.original_name
        setSupportActionBar(toolbar)

        val overview = findViewById<TextView>(R.id.movieOverview)
        val date = findViewById<TextView>(R.id.date)
        val profilePic =findViewById<ImageView>(R.id.movieImage)
        val popularity =findViewById<TextView>(R.id.popularity)
        val votes = findViewById<TextView>(R.id.votes)
        val imageUri ="https://image.tmdb.org/t/p/w500/"+ movie.backdrop_path

        Picasso.with(this).load(imageUri).into(profilePic)
        overview.text = movie.overview
        "Released: ${movie.release_date ?: "2021-01-03"}".also { date.text = it }
        "Popularity: ${movie.popularity}".also { popularity.text = it }
        "Votes: ${movie.vote_count}".also { votes.text = it }
    }

}