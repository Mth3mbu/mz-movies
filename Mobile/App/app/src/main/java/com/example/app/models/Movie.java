package com.example.app.models;

import com.example.app.MoviesResult;

public class Movie {
    public int page;
    public MoviesResult[] results;
    public int total_pages;
    public int total_results;

    public Movie(int page, MoviesResult[] results, int total_pages, int total_results){
        this.page =page;
        this.results =results;
        this.total_pages =total_pages;
        this.total_results =total_results;
    }
}
