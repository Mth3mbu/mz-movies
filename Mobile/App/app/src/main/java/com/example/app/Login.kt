package com.example.app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class Login : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val lblRegister = findViewById<TextView>(R.id.btnRegister)
        val btnLogin =  findViewById<Button>(R.id.btnLogin)

        lblRegister.setOnClickListener{
            val intent =Intent(this, Register::class.java)
            startActivity(intent)
        }

        btnLogin.setOnClickListener{
            val intent =Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }
}